<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\FactoryAdapterMagento;

use Interactiv4\Contracts\Factory\Api\ObjectFactoryInterface;
use Magento\Framework\ObjectManagerInterface;

/**
 * Class ObjectFactory.
 *
 * @api
 */
class ObjectFactory implements ObjectFactoryInterface
{
    /**
     * @var ObjectManagerInterface
     */
    private $objectManager;

    /**
     * ObjectFactory constructor.
     *
     * @param ObjectManagerInterface $objectManager
     */
    public function __construct(
        ObjectManagerInterface $objectManager
    ) {
        $this->objectManager = $objectManager;
    }

    /**
     * {@inheritdoc}
     */
    public function create(
        string $type,
        array $arguments = []
    ) {
        return $this->objectManager->create($type, $arguments);
    }
}
