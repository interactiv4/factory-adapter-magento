# Interactiv4 Factory Adapter Magento

Description
-----------
This library adapts interactiv4/factory contracts to Magento by using and adapting a interactiv4/factory-contracts-implementation package.


Versioning
----------
This package follows semver versioning.


Compatibility
-------------
- PHP ^7.0
- Magento >= 2.2


Installation Instructions
-------------------------
You can install this package using composer by adding it to your composer file using following command:

`composer require interactiv4/factory-adapter-magento --update-with-all-dependencies`


Support
-------
Refer to [issue tracker](https://bitbucket.org/interactiv4/factory-adapter-magento/issues) to open an issue if needed.


Credits
---------
Supported and maintained by Interactiv4 Team.


Contribution
------------
Any contribution is highly appreciated.
The best way to contribute code is to open a [pull request on Bitbucket](https://bitbucket.org/interactiv4/factory-adapter-magento/pull-requests/new).


License
-------
[MIT](https://es.wikipedia.org/wiki/Licencia_MIT)


Copyright
---------
Copyright (c) 2020 Interactiv4 S.L.